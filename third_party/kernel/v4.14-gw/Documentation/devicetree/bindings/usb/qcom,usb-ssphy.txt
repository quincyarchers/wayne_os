Qualcomm Synopsys 1.0.0 SS phy controller
===========================================

Synopsys 1.0.0 ss phy controller supports SS usb connectivity on Qualcomm
chipsets

Required properties:

- compatible:
    Value type: <string>
    Definition: Should contain "qcom,usb-ssphy".

- reg:
    Value type: <prop-encoded-array>
    Definition: USB PHY base address and length of the register map.

- #phy-cells:
    Value type: <u32>
    Definition: Should be 0. See phy/phy-bindings.txt for details.

- clocks:
    Value type: <prop-encoded-array>
    Definition: See clock-bindings.txt section "consumers". List of
		 three clock specifiers for reference, phy core and
		 pipe clocks.

- clock-names:
    Value type: <string>
    Definition: Names of the clocks in 1-1 correspondence with the "clocks"
		 property. Must contain "ref", "phy" and "pipe".

- vdd-supply:
    Value type: <phandle>
    Definition: phandle to the regulator VDD supply node.

- vdda1p8-supply:
    Value type: <phandle>
    Definition: phandle to the regulator 1.8V supply node.

- qcom,vdd-voltage-level:
    Value type: <prop-array>
    Definition: This is a list of three integer values <no min max> where
		 each value corresponding to voltage corner in uV.

Optional child nodes:

- vbus-supply:
    Value type: <phandle>
    Definition: phandle to the VBUS supply node.

- resets:
    Value type: <prop-encoded-array>
    Definition: See reset.txt section "consumers". PHY reset specifiers
		 for phy core and COR resets.

- reset-names:
    Value type: <string>
    Definition: Names of the resets in 1-1 correspondence with the "resets"
		 property. Must contain "com" and "phy".

Example:

usb3_phy: phy@78000 {
	compatible = "qcom,usb-ssphy";
	reg = <0x78000 0x400>;
	#phy-cells = <0>;
	clocks = <&rpmcc RPM_SMD_LN_BB_CLK>,
		 <&gcc GCC_USB_HS_PHY_CFG_AHB_CLK>,
		 <&gcc GCC_USB3_PHY_PIPE_CLK>;
	clock-names = "ref", "phy", "pipe";
	resets = <&gcc GCC_USB3_PHY_BCR>,
		 <&gcc GCC_USB3PHY_PHY_BCR>;
	reset-names = "com", "phy";
	vdd-supply = <&vreg_l3_1p05>;
	vdda1p8-supply = <&vreg_l5_1p8>;
	vbus-supply = <&usb3_vbus_reg>;
	qcom,vdd-voltage-level = <0 1050000 1050000>;
};
