%# Copyright (c) 2012 The Chromium OS Authors. All rights reserved.
%# Use of this source code is governed by a BSD-style license that can be
%# found in the LICENSE file.

%import pprint

%from src import settings
%_root = settings.settings.relative_root

%def body_block():
  %# --------------------------------------------------------------------------
  %# Switcher toolbar allows query parameter removal.
  %include('switcher_bar.tpl', tpl_vars=tpl_vars, query_string=query_string, url_base='filters')

  %# --------------------------------------------------------------------------
  %# Filters
  %if not tpl_vars.get('filters'):
    <h3><u>No filters found.</u></h3>
  %else:
    <div id="divFilters" align="left">
      <table>
        <tbody>
          <tr>
            <th class="headeritem centered">
              Filter Name<br>
              (selectable)
            </th>
            <th class="headeritem lefted">Filter Definition</th>
          </tr>
          %_filters = tpl_vars['filters']
          %_filter_keys = _filters.keys()
          %if 'unfiltered' in _filter_keys:
            %_filter_keys.remove('unfiltered')
          %end
          %for filter_name in ['unfiltered'] + sorted(_filter_keys):
            <tr>
              <td class="centered"
                  onmouseover="fixTooltip(event, this)"
                  onmouseout="clearTooltip(this)">
                <a class="tooltip"
                   href="{{ _root }}/{{ filter_name }}">
                  {{ filter_name }}
                  <span style="width:300px">
                    Jump to the default view with filter={{ filter_name }}.
                  </span>
                </a>
              </td>
              <td class="lefted">
                {{! pprint.pformat(_filters[filter_name]) }}
              </td>
            </tr>
          %end
        </tbody>
      </table>
    </div>
    <a class="maia-button" href="{{ _root }}/tempfilter" target="_blank">
      Create a temporary filter
    </a>
  %end
%end

%rebase('master.tpl', title='filters', query_string=query_string, body_block=body_block)
