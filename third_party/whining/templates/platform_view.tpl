%# Copyright (c) 2012 The Chromium OS Authors. All rights reserved.
%# Use of this source code is governed by a BSD-style license that can be
%# found in the LICENSE file.

%from src import settings
%_root = settings.settings.relative_root


%# Tooltip used when mouse is over the run numbers
%def tooltip(nfail, nother, build, test_name, crash_count=0, crashes=[]):
  <span style="width:300px">
    <div class="text_popup_title">{{ test_name }}:</div>
    %if nfail > 0:
      <div class="text_popup_fail">
        View failure details.
      </div>
    %elif nother > 0:
      <div class="text_popup_problem">
        View problem details.
      </div>
    %else:
      View raw test logs on {{ build }}.
    %end
    %if crash_count:
      <div class="text_popup_crash">
        {{ crash_count }} Crashes:</div>
        %for test_name, crash_info in crashes:
          {{ test_name }}:<br>
          %for one_crash in crash_info:
            &nbsp;&nbsp;{{ one_crash }}<br>
          %end
        %end
    %end
  </span>
%end


%def body_block():
  %# --------------------------------------------------------------------------
  %# Releases switcher
  %include('switcher_bar.tpl', tpl_vars=tpl_vars, query_string=query_string, url_base='platform')

  <div id="divPlatforms" class="centered">
  %_td = tpl_vars['data']['table_data']
  %_experimental_test_names = tpl_vars['data']['experimental_test_names']
  %_filter_tag = tpl_vars['filter_tag']
  %_show_faft_view = tpl_vars['data']['show_faft_view']
  %if not _td.row_headers:
    <h3><u>No test results found.</u></h3>
  %else:
    %# -------------------------------------------------------------------------
    %# LEGEND
    %include('platform_legend.tpl')

    %# --------------------------------------------------------------------------
    %# Show the test runs
    <table class="alternate_background" style="width:100%">
      <thead>
        <tr>
          <th class="headeritem th_testname bottomed">
            {{ _td.row_label }}/{{ _td.col_label }}
          </th>
          %if _show_faft_view:
            <th class="headeritem th_testname bottomed">FW RW Ver</th>
            <th class="headeritem th_testname bottomed">FW RO Ver</th>
            <th class="headeritem th_testname bottomed">Test Ver</th>
          %end
          <th class="headeritem th_testname bottomed">Jobs</th>
          %for _test_name in _td.col_headers:
            %# Would be cleaner to move tooltip content into celldata class.
            %# Would allow for dynamic table transpose.
            %if _test_name in _experimental_test_names:
              <th class="centered th_testname experimental"
            %else:
              <th class="centered th_testname"
            %end
                onmouseover="fixTooltip(event, this)" onmouseout="clearTooltip(this)">
              <div class="div_testname">
              <a href="{{ _root }}/summary/{{ _filter_tag }}{{! query_string(add={'tests': _test_name}, remove=['platforms']) }}">
                %if len(_test_name) > 23:
                  %_test_name = '%s...' % _test_name[:23]
                %end
                {{ _test_name }}
              </a>
              </div>
            </th>
          %end
        </tr>
      </thead>
      <tbody>
        %for _build in _td.row_headers:
          <tr>
            <th class="headeritem">{{ _build }}</td>
            %if _show_faft_view:
              %_cell = _td.get_cell(_build, _td.col_headers[0])
              <th class="centered">{{_cell.fw_rw_version}}</th>
              <th class="centered">{{_cell.fw_ro_version}}</th>
              <th class="centered">{{_cell.test_version}}</th>
            %end
            <th>
              <a href="{{ _root }}/jobs/{{ _filter_tag }}{{ query_string(add={'builds': _build}) }}">
                jobs
              </a>
            </th>
            %for _test_name in _td.col_headers:
              <td>
                %_cell = _td.get_cell(_build, _test_name)
                %_test_ids = ','.join(str(id) for id in _cell.test_ids)
                %_testrun = '/testrun/%s?test_ids=%s' % (_filter_tag, _test_ids)
                %if _cell.nattempted == 0:
                  <a class="undecorated tooltip" href="">
                    &nbsp;
                    <span style="width:300px">
                      <b>{{ _test_name }}</b> not attempted in build {{ _build }}.
                    </span>
                  </a>
                  %continue
                %end
                <table style="width:100%;">
                  <tbody>
                  <tr>
                  %crashes, crash_count = ([], 0)  # TODO: get crash data
                  %if _cell.ngood > 0:
                    <td class="bordereditem success"
                        onmouseover="fixTooltip(event, this)" onmouseout="clearTooltip(this)"
                        style="width:{{ 100.0 * _cell.ngood / _cell.nattempted }}%;">
                      <a class="tooltip" target="_blank"
                         href="{{ _root }}{{ _testrun }}">
                        {{ _cell.ngood }}
                        %tooltip(0, 0, _build, _test_name, crash_count, crashes)
                      </a>
                    </td>
                  %end
                  %if _cell.nfail > 0 or _cell.nother > 0:
                      %_qs = query_string(add={'builds': _build, 'tests': _test_name})
                  %end
                  %if _cell.nfail > 0:
                    <td class="bordereditem failure"
                        onmouseover="fixTooltip(event, this)" onmouseout="clearTooltip(this)"
                        style="width:{{ 100.0 * _cell.nfail / _cell.nattempted }}%;">
                      <a class="tooltip"
                      href="{{ _root }}/failures/{{ _filter_tag }}{{! _qs }}">
                        {{ _cell.nfail }}
                        %tooltip(_cell.nfail, 0, _build, _test_name, crash_count, crashes)
                      </a>
                    </td>
                  %end
                  %if _cell.nother > 0:
                    <td class="bordereditem warning_summary"
                        onmouseover="fixTooltip(event, this)" onmouseout="clearTooltip(this)"
                        style="width:{{ 100.0 * _cell.nother / _cell.nattempted }}%;">
                      <a class="tooltip"
                      href="{{ _root }}/failures/{{ _filter_tag }}{{! _qs }}">
                        {{ _cell.nother }}
                        %tooltip(0, _cell.nother, _build, _test_name, crash_count, crashes)
                      </a>
                    </td>
                  %end
                  %if crash_count:
                    <td class="bordereditem warning_summary">
                      {{ crash_count }}
                    </td>
                  %end
                  </tr>
                  </tbody>
                </table>
              </td>
            %end
          </tr>
        %end
      </tbody>
    </table>
  %end

  </div>
%end

%rebase('master.tpl', title='Platform Test Summary', query_string=query_string, body_block=body_block)
