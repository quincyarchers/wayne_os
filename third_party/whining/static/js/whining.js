/* Copyright (c) 2012 The Chromium OS Authors. All rights reserved.
 * Use of this source code is governed by a BSD-style license that can be
 * found in the LICENSE file.
 */
function fixTooltip(event, box) {
  e = box.getElementsByTagName('span')[0];

  // Show the tooltip so we can get the width.
  e.style.display = 'block';

  // Use a slight offset so user can move to the next cell unhindered.
  offset = 5;

  // If the tooltip falls off the right side, move it left of mouse.
  x = event.clientX;
  if (x + e.offsetWidth > window.innerWidth)
    x = x - offset - e.offsetWidth;
  else
    x = x + offset;

  // If the tooltip falls off the bottom, move it above the mouse.
  y = event.clientY;
  if (y + e.offsetHeight > window.innerHeight)
    y = y - offset - e.offsetHeight;
  else
    y = y + offset;
  if (y < 0)
    y = 0;

  e.style.left = x + 'px';
  e.style.top = y + 'px';
}

function clearTooltip(box) {
  box.getElementsByTagName('span')[0].style.display = 'none';
}
