# Copyright 2017 The Chromium OS Authors. All rights reserved.
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.

description   "atrusd"
author        "chromium-os-dev@chromium.org"

start on started system-services
stop on stopping system-services
respawn
respawn limit 3 10  # if the job respawns 3 times in 10 seconds, stop trying.

expect fork

# -u run as user atrus
# -g run as group atrus
# -c CAP_DAC_OVERRIDE is needed in order to access /dev/hidraw*. It would be
#    possible to have an udev rule that granted e.g. group atrus rw permissions.
#    However, there's a rule /lib/udev/rules.d/99-hidraw.rules that would
#    override this.
# -i exit immediately after fork (do not act as init).
# -l enter new IPC namespace
# -p enter new pid namespace
# -n set no_new_privs
# -w create and join a new anonymous session keyring
# -P pivot_root(2) into the chroot
# -t create a new /tmp inside chroot
# -b/-k directories to mount into chroot
#
# -e isn't used because atrusd won't receive libudev events when it's used
exec minijail0 -u atrus -g atrus -c 2 -i -l -p -n -w -t \
     -P /mnt/empty -b / -b /dev -b /sys \
     -k '/run,/run,tmpfs,MS_NODEV|MS_NOEXEC|MS_NOSUID,mode=755,size=10M' \
     -b /run/udev -b /run/dbus -b /run/systemd/journal \
     -- /usr/sbin/atrusd \
     --upgrade_file_path=/lib/firmware/google/atrus-fw-bundle-latest.bin \
     --vmodule=diagnostics=1,udev_device_manager=1,atrusd=1
