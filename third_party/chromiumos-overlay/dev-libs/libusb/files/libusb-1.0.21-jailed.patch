From b541cadad7285054388436fefe7da097d164f19c Mon Sep 17 00:00:00 2001
From: Jingkui Wang <jkwang@google.com>
Date: Thu, 21 Feb 2019 13:44:16 -0800
Subject: [PATCH] patch libusb to make it work in a jailed environment

Libusb would work without usbfs, sysfs or netlink. It will rely on
external fd to get device or open device handle.

This patch adds the following api.
    int libusb_init_jailed(libusb_context **context);
    int libusb_get_device_from_fd(libusb_context* ctx, int fd,
            libusb_device** device)

To use libusb in a jailed environment, init with "libusb_init_jailed", and
then get device with "libusb_get_device_from_fd".
---
 libusb/core.c           |  34 +++++++-
 libusb/libusb.h         |   4 +
 libusb/libusbi.h        |  10 +++
 libusb/os/linux_usbfs.c | 179 ++++++++++++++++++++++++++++++++++++++--
 4 files changed, 214 insertions(+), 13 deletions(-)

diff --git a/libusb/core.c b/libusb/core.c
index 8c9b8e9..1512e4f 100644
--- a/libusb/core.c
+++ b/libusb/core.c
@@ -1735,6 +1735,10 @@ out:
 	return r;
 }
 
+int API_EXPORTED libusb_get_device_from_fd(libusb_context *ctx, int fd, libusb_device** device) {
+	return usbi_backend->get_device_from_fd(ctx, fd, device);
+}
+
 /** \ingroup libusb_dev
  * Activate an alternate setting for an interface. The interface must have
  * been previously claimed with libusb_claim_interface().
@@ -2115,6 +2119,8 @@ void API_EXPORTED libusb_set_debug(libusb_context *ctx, int level)
 		ctx->debug = level;
 }
 
+int libusb_init_helper(libusb_context **context, int jailed);
+
 /** \ingroup libusb_lib
  * Initialize libusb. This function must be called before calling any other
  * libusb function.
@@ -2129,6 +2135,16 @@ void API_EXPORTED libusb_set_debug(libusb_context *ctx, int level)
  * \see libusb_contexts
  */
 int API_EXPORTED libusb_init(libusb_context **context)
+{
+	return libusb_init_helper(context, 0);
+}
+
+int API_EXPORTED libusb_init_jailed(libusb_context **context)
+{
+	return libusb_init_helper(context, 1);
+}
+
+int libusb_init_helper(libusb_context **context, int jailed)
 {
 	struct libusb_device *dev, *next;
 	char *dbg = getenv("LIBUSB_DEBUG");
@@ -2190,10 +2206,20 @@ int API_EXPORTED libusb_init(libusb_context **context)
 	list_add (&ctx->list, &active_contexts_list);
 	usbi_mutex_static_unlock(&active_contexts_lock);
 
-	if (usbi_backend->init) {
-		r = usbi_backend->init(ctx);
-		if (r)
-			goto err_free_ctx;
+	if (jailed) {
+		ctx->jailed = 1;
+		if (usbi_backend->init_jailed) {
+			r = usbi_backend->init_jailed(ctx);
+			if (r)
+				goto err_free_ctx;
+		}
+	} else {
+		ctx->jailed = 0;
+		if (usbi_backend->init) {
+			r = usbi_backend->init(ctx);
+			if (r)
+				goto err_free_ctx;
+		}
 	}
 
 	r = usbi_io_init(ctx);
diff --git a/libusb/libusb.h b/libusb/libusb.h
index 34d0784..8395dd4 100644
--- a/libusb/libusb.h
+++ b/libusb/libusb.h
@@ -1304,6 +1304,7 @@ enum libusb_log_level {
 };
 
 int LIBUSB_CALL libusb_init(libusb_context **ctx);
+int LIBUSB_CALL libusb_init_jailed(libusb_context **ctx);
 void LIBUSB_CALL libusb_exit(libusb_context *ctx);
 void LIBUSB_CALL libusb_set_debug(libusb_context *ctx, int level);
 const struct libusb_version * LIBUSB_CALL libusb_get_version(void);
@@ -1386,6 +1387,9 @@ int LIBUSB_CALL libusb_release_interface(libusb_device_handle *dev_handle,
 libusb_device_handle * LIBUSB_CALL libusb_open_device_with_vid_pid(
 	libusb_context *ctx, uint16_t vendor_id, uint16_t product_id);
 
+int LIBUSB_CALL libusb_get_device_from_fd(
+	libusb_context *ctx, int fd, libusb_device** device);
+
 int LIBUSB_CALL libusb_set_interface_alt_setting(libusb_device_handle *dev_handle,
 	int interface_number, int alternate_setting);
 int LIBUSB_CALL libusb_clear_halt(libusb_device_handle *dev_handle,
diff --git a/libusb/libusbi.h b/libusb/libusbi.h
index ca5e86a..cd3230e 100644
--- a/libusb/libusbi.h
+++ b/libusb/libusbi.h
@@ -256,6 +256,7 @@ struct pollfd;
 struct libusb_context {
 	int debug;
 	int debug_fixed;
+	int jailed;
 
 	/* internal event pipe, used for signalling occurrence of an internal event. */
 	int event_pipe[2];
@@ -367,6 +368,7 @@ struct libusb_device {
 	 * time */
 	usbi_mutex_t lock;
 	int refcnt;
+	int handle;
 
 	struct libusb_context *ctx;
 
@@ -607,6 +609,12 @@ struct usbi_os_backend {
 	 */
 	int (*init)(struct libusb_context *ctx);
 
+	/* Init libusb for a jailed environment. Libusb won't have access to sysfs,
+	 * usbfs or netlink. It depends on external fd of usbfs to open device
+	 * handle.
+	 */
+	int (*init_jailed)(struct libusb_context *ctx);
+
 	/* Deinitialization. Optional. This function should destroy anything
 	 * that was set up by init.
 	 *
@@ -614,6 +622,8 @@ struct usbi_os_backend {
 	 */
 	void (*exit)(void);
 
+	int (*get_device_from_fd)(libusb_context *ctx, int fd, libusb_device** device);
+
 	/* Enumerate all the USB devices on the system, returning them in a list
 	 * of discovered devices.
 	 *
diff --git a/libusb/os/linux_usbfs.c b/libusb/os/linux_usbfs.c
index 89102ef..1f7b2d4 100644
--- a/libusb/os/linux_usbfs.c
+++ b/libusb/os/linux_usbfs.c
@@ -187,6 +187,10 @@ static int _get_usbfs_fd(struct libusb_device *dev, mode_t mode, int silent)
 	int fd;
 	int delay = 10000;
 
+	if (dev->handle) {
+		return dup(dev->handle);
+	}
+
 	if (!usbfs_path) {
 		usbi_err(ctx, "could not find usbfs");
 		return LIBUSB_ERROR_OTHER;
@@ -463,6 +467,41 @@ static int op_init(struct libusb_context *ctx)
 
 	return r;
 }
+static int op_init_jailed(struct libusb_context *ctx)
+{
+	struct stat statbuf;
+
+	if (monotonic_clkid == -1)
+		monotonic_clkid = find_monotonic_clock();
+
+	if (supports_flag_bulk_continuation == -1) {
+		/* bulk continuation URB flag available from Linux 2.6.32 */
+		supports_flag_bulk_continuation = kernel_version_ge(2,6,32);
+		if (supports_flag_bulk_continuation == -1) {
+			usbi_err(ctx, "error checking for bulk continuation support");
+			return LIBUSB_ERROR_OTHER;
+		}
+	}
+
+	if (supports_flag_bulk_continuation)
+		usbi_dbg("bulk continuation flag supported");
+
+	if (-1 == supports_flag_zero_packet) {
+		/* zero length packet URB flag fixed since Linux 2.6.31 */
+		supports_flag_zero_packet = kernel_version_ge(2,6,31);
+		if (-1 == supports_flag_zero_packet) {
+			usbi_err(ctx, "error checking for zero length packet support");
+			return LIBUSB_ERROR_OTHER;
+		}
+	}
+
+	if (supports_flag_zero_packet)
+		usbi_dbg("zero length packet flag supported");
+
+	sysfs_can_relate_devices = 0;
+	sysfs_has_descriptors = 0;
+	return LIBUSB_SUCCESS;
+}
 
 static void op_exit(void)
 {
@@ -895,6 +934,7 @@ static int initialize_device(struct libusb_device *dev, uint8_t busnum,
 	int fd, speed;
 	ssize_t r;
 
+	dev->handle = 0;
 	dev->bus_number = busnum;
 	dev->device_address = devaddr;
 
@@ -987,6 +1027,79 @@ static int initialize_device(struct libusb_device *dev, uint8_t busnum,
 	return r;
 }
 
+static int initialize_device_jailed(struct libusb_device *dev, int fd_in)
+{
+	struct linux_device_priv *priv = _device_priv(dev);
+	struct libusb_context *ctx = DEVICE_CTX(dev);
+	int descriptors_size = 512; /* Doubled to an initial 1024 byte alloc */
+	int fd, speed;
+	ssize_t r;
+
+	dev->handle = fd_in;
+	dev->bus_number = 0;
+	dev->device_address = 0;
+
+	fd = _get_usbfs_fd(dev, O_RDONLY, 0);
+	if (fd < 0)
+		return fd;
+
+	do {
+		descriptors_size *= 2;
+		priv->descriptors = usbi_reallocf(priv->descriptors,
+						  descriptors_size);
+		if (!priv->descriptors) {
+			close(fd);
+			return LIBUSB_ERROR_NO_MEM;
+		}
+		/* usbfs has holes in the file */
+		memset(priv->descriptors + priv->descriptors_len,
+				0, descriptors_size - priv->descriptors_len);
+		r = read(fd, priv->descriptors + priv->descriptors_len,
+			 descriptors_size - priv->descriptors_len);
+		if (r < 0) {
+			usbi_err(ctx, "read descriptor failed ret=%d errno=%d",
+				 fd, errno);
+			close(fd);
+			return LIBUSB_ERROR_IO;
+		}
+		priv->descriptors_len += r;
+	} while (priv->descriptors_len == descriptors_size);
+
+	close(fd);
+
+	if (priv->descriptors_len < DEVICE_DESC_LENGTH) {
+		usbi_err(ctx, "short descriptor read (%d)",
+			 priv->descriptors_len);
+		return LIBUSB_ERROR_IO;
+	}
+
+	/* cache active config */
+	fd = _get_usbfs_fd(dev, O_RDWR, 1);
+	if (fd < 0) {
+		/* cannot send a control message to determine the active
+		 * config. just assume the first one is active. */
+		usbi_warn(ctx, "Missing rw usbfs access; cannot determine "
+			       "active configuration descriptor");
+		if (priv->descriptors_len >=
+				(DEVICE_DESC_LENGTH + LIBUSB_DT_CONFIG_SIZE)) {
+			struct libusb_config_descriptor config;
+			usbi_parse_descriptor(
+				priv->descriptors + DEVICE_DESC_LENGTH,
+				"bbwbbbbb", &config, 0);
+			priv->active_config = config.bConfigurationValue;
+		} else
+			priv->active_config = -1; /* No config dt */
+
+		return LIBUSB_SUCCESS;
+	}
+
+	r = usbfs_get_active_config(dev, fd);
+	close(fd);
+
+	return r;
+}
+
+
 static int linux_get_parent_info(struct libusb_device *dev, const char *sysfs_dir)
 {
 	struct libusb_context *ctx = DEVICE_CTX(dev);
@@ -1111,11 +1224,9 @@ void linux_hotplug_enumerate(uint8_t busnum, uint8_t devaddr, const char *sys_na
 	usbi_mutex_static_unlock(&active_contexts_lock);
 }
 
-void linux_device_disconnected(uint8_t busnum, uint8_t devaddr)
-{
+static void linux_device_disconnected_session_id(unsigned long session_id) {
 	struct libusb_context *ctx;
 	struct libusb_device *dev;
-	unsigned long session_id = busnum << 8 | devaddr;
 
 	usbi_mutex_static_lock(&active_contexts_lock);
 	list_for_each_entry(ctx, &active_contexts_list, list, struct libusb_context) {
@@ -1130,6 +1241,13 @@ void linux_device_disconnected(uint8_t busnum, uint8_t devaddr)
 	usbi_mutex_static_unlock(&active_contexts_lock);
 }
 
+void linux_device_disconnected(uint8_t busnum, uint8_t devaddr)
+{
+	unsigned long session_id = busnum << 8 | devaddr;
+
+	linux_device_disconnected_session_id(session_id);
+}
+
 #if !defined(USE_UDEV)
 /* open a bus directory and adds all discovered devices to the context */
 static int usbfs_scan_busdir(struct libusb_context *ctx, uint8_t busnum)
@@ -1292,6 +1410,41 @@ static int linux_default_scan_devices (struct libusb_context *ctx)
 }
 #endif
 
+static int op_get_device_from_fd(libusb_context *ctx,
+		int fd, libusb_device** device) {
+	unsigned long session_id;
+	libusb_device* dev;
+	int r;
+
+	if (!ctx->jailed) {
+		return LIBUSB_ERROR_OTHER;
+	}
+
+	session_id = ctx->jailed ++;
+	dev = usbi_alloc_device(ctx, session_id);
+	if (!dev)
+		return LIBUSB_ERROR_NO_MEM;
+
+	r = initialize_device_jailed(dev, fd);
+	dev->bus_number = session_id >> 8;
+	dev->device_address = session_id & 0xff;
+
+	if (r < 0)
+		goto out;
+	r = usbi_sanitize_device(dev);
+	if (r < 0)
+		goto out;
+out:
+	if (r < 0)
+		libusb_unref_device(dev);
+	else {
+		usbi_connect_device(dev);
+		*device = dev;
+	}
+
+	return r;
+
+}
 static int op_open_fd(struct libusb_device_handle *handle, int fd)
 {
 	struct linux_device_handle_priv *hpriv = _device_handle_priv(handle);
@@ -2642,7 +2795,9 @@ static int op_handle_events(struct libusb_context *ctx,
 			continue;
 		}
 
-		if (pollfd->revents & POLLERR) {
+		if ((pollfd->revents & POLLERR)
+				|| (pollfd->revents & POLLHUP)
+				|| (pollfd->revents & POLLNVAL)) {
 			/* remove the fd from the pollfd set so that it doesn't continuously
 			 * trigger an event, and flag that it has been removed so op_close()
 			 * doesn't try to remove it a second time */
@@ -2651,11 +2806,15 @@ static int op_handle_events(struct libusb_context *ctx,
 
 			/* device will still be marked as attached if hotplug monitor thread
 			 * hasn't processed remove event yet */
-			usbi_mutex_static_lock(&linux_hotplug_lock);
-			if (handle->dev->attached)
-				linux_device_disconnected(handle->dev->bus_number,
-						handle->dev->device_address);
-			usbi_mutex_static_unlock(&linux_hotplug_lock);
+			if (ctx->jailed) {
+				linux_device_disconnected_session_id(handle->dev->session_data);
+			} else {
+				usbi_mutex_static_lock(&linux_hotplug_lock);
+				if (handle->dev->attached)
+					linux_device_disconnected(handle->dev->bus_number,
+							handle->dev->device_address);
+				usbi_mutex_static_unlock(&linux_hotplug_lock);
+			}
 
 			if (hpriv->caps & USBFS_CAP_REAP_AFTER_DISCONNECT) {
 				do {
@@ -2706,7 +2865,9 @@ const struct usbi_os_backend linux_usbfs_backend = {
 	.name = "Linux usbfs",
 	.caps = USBI_CAP_HAS_HID_ACCESS|USBI_CAP_SUPPORTS_DETACH_KERNEL_DRIVER,
 	.init = op_init,
+	.init_jailed = op_init_jailed,
 	.exit = op_exit,
+	.get_device_from_fd = op_get_device_from_fd,
 	.get_device_list = NULL,
 	.hotplug_poll = op_hotplug_poll,
 	.get_device_descriptor = op_get_device_descriptor,
-- 
2.21.0.rc2.261.ga7da99ff1b-goog

