# Copyright 2018 The Chromium OS Authors. All rights reserved.
# Distributed under the terms of the GNU General Public License v2

EAPI="5"

CROS_WORKON_COMMIT="554ffb5b502ab274904afaee72f7328a992bbfe3"
CROS_WORKON_TREE=("b050a2ab2836dd6da5e48eab3fd4ac328d4325bc" "1a0304cbbffe1ca5fe2379d1ac33495053ea6a24" "e7dba8c91c1f3257c34d4a7ffff0ea2537aeb6bb")
CROS_WORKON_INCREMENTAL_BUILD=1
CROS_WORKON_LOCALNAME="platform2"
CROS_WORKON_PROJECT="chromiumos/platform2"
CROS_WORKON_OUTOFTREE_BUILD=1
CROS_WORKON_SUBTREE="common-mk dlcservice .gn"

PLATFORM_SUBDIR="dlcservice/client"

inherit cros-workon platform

DESCRIPTION="DlcService D-Bus client library for Chromium OS"
HOMEPAGE="https://chromium.googlesource.com/chromiumos/platform2/+/master/dlcservice/client"

LICENSE="BSD-Google"
SLOT="0"
KEYWORDS="*"

# D-Bus proxies generated by this client library depend on the code generator
# itself (chromeos-dbus-bindings) and produce header files that rely on
# libbrillo library.
DEPEND="
	chromeos-base/chromeos-dbus-bindings
"

src_install() {
	# Install DBus client library.
	platform_install_dbus_client_lib "dlcservice"
}
