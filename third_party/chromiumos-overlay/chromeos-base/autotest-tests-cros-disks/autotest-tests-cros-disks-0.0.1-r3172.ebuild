# Copyright 2014 The Chromium OS Authors. All rights reserved.
# Distributed under the terms of the GNU General Public License v2

EAPI=5
CROS_WORKON_COMMIT="8e4f9e887204e08d6f341a0af99288ca8c38b762"
CROS_WORKON_TREE="119b5f58f2d190d8382cd28ddfd23dcfb64d687d"
CROS_WORKON_PROJECT="chromiumos/third_party/autotest"
CROS_WORKON_LOCALNAME=../third_party/autotest/files

inherit cros-workon autotest

DESCRIPTION="cros-disks autotests"
HOMEPAGE="http://www.chromium.org/"
SRC_URI=""

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="*"
# Enable autotest by default.
IUSE="+autotest"

RDEPEND="
	!<chromeos-base/autotest-tests-0.0.3
"
DEPEND="${RDEPEND}"

IUSE_TESTS="
	+tests_platform_CrosDisksArchive
	+tests_platform_CrosDisksDBus
	+tests_platform_CrosDisksFilesystem
	+tests_platform_CrosDisksFormat
	+tests_platform_CrosDisksRename
	+tests_platform_CrosDisksSshfs
"

IUSE="${IUSE} ${IUSE_TESTS}"

AUTOTEST_FILE_MASK="*.a *.tar.bz2 *.tbz2 *.tgz *.tar.gz"
