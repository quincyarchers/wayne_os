// Copyright 2015 The Weave Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#ifndef LIBWEAVE_SRC_DEVICE_MANAGER_H_
#define LIBWEAVE_SRC_DEVICE_MANAGER_H_

#include <base/memory/weak_ptr.h>
#include <weave/device.h>

namespace weave {

class BaseApiHandler;
class Config;
class CommandManager;
class DeviceRegistrationInfo;
class StateChangeQueue;
class StateManager;

namespace privet {
class Manager;
}  // namespace privet

class DeviceManager final : public Device {
 public:
  DeviceManager(provider::ConfigStore* config_store,
                provider::TaskRunner* task_runner,
                provider::HttpClient* http_client,
                provider::Network* network,
                provider::DnsServiceDiscovery* dns_sd,
                provider::HttpServer* http_server,
                provider::Wifi* wifi,
                provider::Bluetooth* bluetooth);
  ~DeviceManager() override;

  // Device implementation.
  const Settings& GetSettings() const override;
  void AddSettingsChangedCallback(
      const SettingsChangedCallback& callback) override;
  void AddCommandDefinitionsFromJson(const std::string& json) override;
  void AddCommandDefinitions(const base::DictionaryValue& dict) override;
  bool AddCommand(const base::DictionaryValue& command,
                  std::string* id,
                  ErrorPtr* error) override;
  Command* FindCommand(const std::string& id) override;
  void AddCommandHandler(const std::string& command_name,
                         const CommandHandlerCallback& callback) override;
  void AddStateChangedCallback(const base::Closure& callback) override;
  void AddStateDefinitionsFromJson(const std::string& json) override;
  void AddStateDefinitions(const base::DictionaryValue& dict) override;
  bool SetStatePropertiesFromJson(const std::string& json,
                                  ErrorPtr* error) override;
  bool SetStateProperties(const base::DictionaryValue& dict,
                          ErrorPtr* error) override;
  std::unique_ptr<base::Value> GetStateProperty(
      const std::string& name) const override;
  bool SetStateProperty(const std::string& name,
                        const base::Value& value,
                        ErrorPtr* error) override;
  std::unique_ptr<base::DictionaryValue> GetState() const override;
  void Register(const std::string& ticket_id,
                const DoneCallback& callback) override;
  void SetXmppChannel(const std::string& channel) override;
  GcdState GetGcdState() const override;
  void AddGcdStateChangedCallback(
      const GcdStateChangedCallback& callback) override;
  void AddPairingChangedCallbacks(
      const PairingBeginCallback& begin_callback,
      const PairingEndCallback& end_callback) override;

  Config* GetConfig();

 private:
  void StartPrivet(provider::TaskRunner* task_runner,
                   provider::Network* network,
                   provider::DnsServiceDiscovery* dns_sd,
                   provider::HttpServer* http_server,
                   provider::Wifi* wifi,
                   provider::Bluetooth* bluetooth);

  std::unique_ptr<StateChangeQueue> state_change_queue_;
  std::shared_ptr<StateManager> state_manager_;
  std::shared_ptr<CommandManager> command_manager_;
  std::unique_ptr<DeviceRegistrationInfo> device_info_;
  std::unique_ptr<BaseApiHandler> base_api_handler_;
  std::unique_ptr<privet::Manager> privet_;

  base::WeakPtrFactory<DeviceManager> weak_ptr_factory_{this};
  DISALLOW_COPY_AND_ASSIGN(DeviceManager);
};

}  // namespace weave

#endif  // LIBWEAVE_SRC_DEVICE_MANAGER_H_
