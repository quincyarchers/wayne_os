# Copyright (c) 2014 The Chromium OS Authors. All rights reserved.
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.

description   "Configure DHCP server for moblab subnet"
author        "chromium-os-dev@chromium.org"

normal exit 0

script
  # Preparing dhcp's /var/{lib|run} dirs to run as user 'dhcp'.
  mkdir -p /var/log/bootup/
  exec >>/var/log/bootup/${UPSTART_JOB}.log 2>&1
  set -x
  set -e

  DHCPD_IFACE='lxcbr0'

  logger -t "${UPSTART_JOB}" "preparing lease and pid files for dhcpd"
  mkdir -m 0755 -p /run/dhcp /var/lib/dhcp
  chmod -R u+rwX,g+rX,o+rX /var/lib/dhcp
  chown -R dhcp:dhcp /run/dhcp /var/lib/dhcp

  LEASE_FILE="/var/lib/dhcp/dhcpd.leases"
  PID_FILE="/run/dhcp/dhcpd.pid"
  DHCPD_CONF="/etc/dhcp/dhcpd-moblab.conf"
  DHCPD_RESERVATIONS1="/mnt/moblab/dhcpd-reservations.conf"
  DHCPD_RESERVATIONS2="/mnt/moblab-settings/dhcpd-reservations.conf"
  DHCPD_EXTENSION_DIR="/var/run/moblab/"
  DHCPD_EXTENSION_FILE="${DHCPD_EXTENSION_DIR}dhcpd-moblab.conf"


  logger -t "${UPSTART_JOB}" "${DHCPD_IFACE} will be used for subnet."

  if [ ! -f "${LEASE_FILE}" ] ; then
    touch "${LEASE_FILE}"
    chown dhcp:dhcp "${LEASE_FILE}"
  fi

  mkdir -p "${DHCPD_EXTENSION_DIR}" || :
  rm -f "${DHCPD_EXTENSION_FILE}" || :
  touch "${DHCPD_EXTENSION_FILE}" || :
  chown moblab:moblab "${DHCPD_EXTENSION_FILE}"

  # We have two possible locations whilst we move from guado to
  # fizz based systems, TODO remove the first location when
  # guado is no longer supported
  if [ -e "${DHCPD_RESERVATIONS1}" ] ; then
    echo "include \"${DHCPD_RESERVATIONS1}\";" >> "${DHCPD_EXTENSION_FILE}"
  fi

  if [ -e "${DHCPD_RESERVATIONS2}" ] ; then
    echo "include \"${DHCPD_RESERVATIONS2}\";" >> "${DHCPD_EXTENSION_FILE}"
  fi

  logger -t "${UPSTART_JOB}" "starting DHCP service against ${DHCPD_IFACE}"
  /usr/sbin/dhcpd -user dhcp -group dhcp -cf ${DHCPD_CONF} ${DHCPD_IFACE}
end script
