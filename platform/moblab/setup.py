# Copyright 2019 The Chromium OS Authors. All rights reserved.
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.

from setuptools import setup

setup(
      name='moblab',
      version='0.1',
      packages=["moblab_common"],
      package_dir={'':'src'},
)
