/* Copyright 2018 The Chromium OS Authors. All rights reserved.
 * Use of this source code is governed by a BSD-style license that can be
 * found in the LICENSE file.
 */

#ifndef INCLUDE_CROS_CAMERA_EXPORT_H_
#define INCLUDE_CROS_CAMERA_EXPORT_H_

#define CROS_CAMERA_EXPORT __attribute__((visibility("default")))

#endif  // INCLUDE_CROS_CAMERA_EXPORT_H_
