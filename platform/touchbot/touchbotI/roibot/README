This module consists of a general "roibot" robot controller interface
module in python, along with sub-modules for the following:

	__init__.py	Standard python module glue to allow functional
			separation of the code into different files.

	roibot.py	The module itself, which is exposed directly
			at the top level of the namespace, i.e. its
			elements are referenced as 'roibot' rather than
			as 'roibot.roibot', which would have been a
			lot of annoying typing, and avoided placing
			the basic functionality in the __init__.py
			file.  This is done for both style guildeline
			reasons, and to permit additional similar
			controllers with differing control syntax or
			functionality to be substituted in the future.

	error.py	The error translation module which is capable
			of decoding all error and sub-error states
			reported by the robot controller.  If the
			robot controller changes, this may need to
			change.  Its elements are referenced as
			'roibot.error'.

	motion.py	A high level motion control package, intended
			to be able to function in place of a serial
			"Teaching Pendant" device.  This programatically
			exposes the raw command functionality normally
			exposed via the teaching pendant device.  Like
			the pendant, you are not able to control more
			than a single axis of motion at a time.

			It can be used to directly control robot motion,
			albeit with some jitter in the absolute position.
			This jitter arises due to the timing for the
			polling interval for position reporting and the
			subsequent ability to send a motion stop command
			to the robot.  If you need more precise positioning,
			you will want to use a stored program instead,
			although you will have to achieve the positions
			you store through the use of pendant commands in
			any case, but stored positions will end up as
			more reproducible when in motion.

	program.py	A package for loading programs into the robots
			EEPROM.  This builds on the other packages in
			order to leverage the textWriteSequential() to
			write a series of program steps.  These steps are
			described in the "Sequential text (Write and read
			commands)" section of the programming manual.

			There are two sets of functions encapsulated in
			this module; the first set is to create command
			strings for use in the textWriteSequential() and
			the textReadSequential() functions, as helpers.
			The second set allows incremental sequential
			command writes which will throw a catchable
			exception if they fail.  This greatly simplifies
			the act of programming by moving the error
			handling out to an encapsulating try/catch and
			makes the resulting code much more readable.

End Of Document.
