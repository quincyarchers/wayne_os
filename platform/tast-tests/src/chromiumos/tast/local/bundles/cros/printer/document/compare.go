// Copyright 2019 The Chromium OS Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

// Package document provides common utilities for performing comparison between
// documents produced during testing.
package document

import (
	"context"
	"io/ioutil"
	"regexp"

	"chromiumos/tast/diff"
	"chromiumos/tast/errors"
	"chromiumos/tast/testing"
)

// TODO(crbug.com/973637): Investigate why it is that CUPS is inconsistent on
// settings the values for the "For" and "Title" fields in the resulting PDF.
// Once the root cause is determined and fixed we should perform the PDF
// comparison without stripping the fields.

// cleanPdfRegex used to clear away PDF document fields which cause
// discrepencies when attempting to perform a diff between PDF documents. These
// fields have no bearing on the actual content of the document, so it is safe
// to clear them away.
var cleanPdfRegex = regexp.MustCompile("(?m)" +
	// matches the "ID" embedded in the PDF file which uniquely
	// identifies the document.
	`(\/ID \[<[a-fA-F0-9]+><[a-fA-F0-9]+>\])` +
	// matches the "CreationDate" field embedded in the PDF file.
	`|(\/CreationDate\(D:[0-9]{14}-[0-9]{2}'[0-9]{2}'\))` +
	// matches the "ModDate" field embedded in the PDF file.
	`|(\/ModDate\(D:[0-9]{14}-[0-9]{2}'[0-9]{2}'\))` +
	// matches the "For" comment field which specifies the user that created the
	// file.
	`|(^%%For: \(\w+\)$)` +
	// matches the "Title" comment field which specifies the title of original
	// document.
	`|(^%%Title: \([\w\.]+\)$)` +
	// matches the comment field which specifies the version of poppler used to
	// produce the pdf.
	`|(^%Produced by poppler.*$)`)

// cleanBaseFontRegex is used to clear away the font 'IDs' in the
// FontDescriptor fields which may differ between systems. Thus if two
// FontDescriptor lines refer to the same font we can ignore any difference
// between the IDs.
//
// For example, in the given FontDescriptor field:
//   <</BaseFont/WDZDNS+Symbola/FontDescriptor 23 0 R/Type/Font
//   The "WDZDNS" ID will be removed.
var cleanBaseFontRegex = regexp.MustCompile(
	`(\/BaseFont\/)([A-Z]{6}\+)([a-zA-Z]+\/FontDescriptor)`)

// cleanFontNameRegex is the same as cleanBaseFontRegex except it matches a
// different form of the FontDescriptor fields.
//
// For example, in the given FontDescriptor field:
//   <</Type/FontDescriptor/FontName/ZQPAHQ+Webdings/FontBBox[0 -200 1000 799]/Flags 4
//   The "ZQPAHQ" ID will be removed.
var cleanFontNameRegex = regexp.MustCompile(
	`(\/FontName\/)([A-Z]{6}\+)([a-zA-Z]+\/FontBBox)`)

func cleanFontDescriptors(contents string, re *regexp.Regexp) string {
	return re.ReplaceAllStringFunc(contents,
		func(m string) string {
			parts := re.FindStringSubmatch(m)
			return parts[1] + parts[3]
		})
}

func cleanPdfContents(contents string) string {
	contents = cleanFontDescriptors(contents, cleanBaseFontRegex)
	contents = cleanFontDescriptors(contents, cleanFontNameRegex)
	return cleanPdfRegex.ReplaceAllLiteralString(contents, "")
}

// CompareFileContents compares the string contents given by output and golden
// and returns an error if there are any differences. If there are any
// differences between the given file contents then the results of the diff are
// written to diffPath.
func CompareFileContents(ctx context.Context, output, golden, diffPath string) error {
	output = cleanPdfContents(output)
	golden = cleanPdfContents(golden)

	testing.ContextLog(ctx, "Comparing output with golden file")
	diff, err := diff.Diff(output, golden)
	if err != nil {
		return errors.Wrap(err, "unexpected diff output")
	}
	if diff != "" {
		testing.ContextLog(ctx, "Dumping diff to ", diffPath)
		if err := ioutil.WriteFile(diffPath, []byte(diff), 0644); err != nil {
			testing.ContextLog(ctx, "Failed to dump diff: ", err)
		}
		return errors.New("result file did not match the expected file")
	}
	return nil
}

// CompareFiles loads the contents of the given output and golden files and
// compares them for differences. If there are any differences between the two
// files then an error will be returned and the result of the diff are written
// to diffPath.
func CompareFiles(ctx context.Context, output, golden, diffPath string) error {
	outputBytes, err := ioutil.ReadFile(output)
	if err != nil {
		return errors.Wrapf(err, "failed to read file %s", output)
	}
	goldenBytes, err := ioutil.ReadFile(golden)
	if err != nil {
		return errors.Wrapf(err, "failed to read file %s", golden)
	}
	return CompareFileContents(ctx, string(outputBytes), string(goldenBytes), diffPath)
}
