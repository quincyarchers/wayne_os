# Copyright 2017 The Chromium OS Authors. All rights reserved.
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.

[MESSAGES CONTROL]

# Disable the message, report, category or checker with the given id(s). You
# can either give multiple identifier separated by comma (,) or put this option
# multiple times (only on the command line, not in the configuration file where
# it should appear only once).
# C0103: Invalid name ""
# C0111: Missing docstring
# C0301: Line too long.
# C0302: Too many lines in module (N)
# C0321: More than one statement on a single line
# E1103: Instance has no '*' member (but some types could not be inferred)
# I0011: Locally disabling warning.
# I0012: Locally enabling warning.
# R0201: Method could be a function
# R0902: Too many instance attributes (N/7)
# R0903: Too few public methods (N/2)
# R0911: Too many return statements (N/6)
# R0912: Too many branches (N/12)
# R0913: Too many arguments (N/5)
# R0914: Too many local variables (N/15)
# R0915: Too many statements (N/50)
# W0122: Use of the exec statement
# W0102: Dangerous default value
# W0141: Used builtin function ''
# W0142: Used * or ** magic
# W0212: Access to protected member
# W0311: Bad indentation.
# W0312: Found indentation with tabs instead of spaces
# W0403: Relative import 'constants', should be 'chromite.cbuildbot.constants'
# W0511: Used when a warning note as FIXME or XXX is detected.
# W0622: Redefining built-in
# R0904: Too many public methods
# R0921: Abstract class not referenced.
disable=C0103,C0111,C0301,C0302,C0321,E1103,I0011,I0012,R0201,R0902,R0903,R0911,R0912,R0913,R0914,R0915,W0102,W0122,W0141,W0142,W0212,W0311,W0312,W0403,W0511,W0622,R0904,R0921


[REPORTS]

# Tells whether to display a full report or only the messages
# CHANGE: No report.
reports=no


[TYPECHECK]

# List of members which are set dynamically and missed by pylint inference
# system, and so shouldn't trigger E0201 when accessed.
# CHANGE: Added 'AndReturn', 'InAnyOrder' and 'MultipleTimes' for pymox.
# CHANGE: Added tempdir for @osutils.TempDirDecorator.
generated-members=REQUEST,acl_users,aq_parent,AndReturn,InAnyOrder,MultipleTimes,tempdir


[BASIC]

# Regular expression which should only match correct function names
#
# CHANGE: The ChromiumOS standard is different than PEP-8, so we need to
# redefine this.
#
# Common exceptions to ChromiumOS standard:
# - main: Standard for main function
function-rgx=([A-Z_][a-zA-Z0-9]{2,30}|main)$

# Regular expression which should only match correct method names
#
# CHANGE: The ChromiumOS standard is different than PEP-8, so we need to
# redefine this. Here's what we allow:
# - CamelCaps, starting with a capital letter.  No underscores in function
#   names.  Can also have a "_" prefix (private method) or a "test" prefix
#   (unit test).
# - Methods that look like __xyz__, which are used to do things like
#   __init__, __del__, etc.
# - setUp, tearDown: For unit tests.
method-rgx=((_|test)?[A-Z][a-zA-Z0-9]{2,30}|__[a-z]+__|setUp|tearDown)$


[SIMILARITIES]

# Minimum lines number of a similarity.
min-similarity-lines=8

