#!/bin/sh
# Copyright (c) 2012 The Chromium OS Authors. All rights reserved.
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.

# Generates font source images.

SCRIPT="$(readlink -f "$0")"
SCRIPT_DIR="$(dirname "$SCRIPT")"
FIRST_ASCII_GLYPH="32" # space
LAST_ASCII_GLYPH="127" # delete
COLOR="#505050"
FONT="Noto Sans"
MARGIN="3"

die() {
  echo "ERROR: $*" >&2
  exit 1
}

main() {
  [ "$#" = "1" ] || die "Usage: $0 output_dir"
  local output="$1"
  mkdir -p "$output"

  local glyph="$FIRST_ASCII_GLYPH"
  local c=''
  local fontsize=""
  echo "Generating glyph text source..."
  while [[ "$glyph"  != "$LAST_ASCII_GLYPH" ]]; do
    c=$(printf "\x$(printf %x $glyph)")
    echo "$c" >"$output/idx$(printf "%03d" $glyph)_$(printf "%x" $glyph).txt"
    glyph=$((glyph + 1))
  done

  [ -n "$FONTSIZE" ] && fontsize="--point=$FONTSIZE"

  echo "Converting glyph images..."
  "$SCRIPT_DIR/text_to_png_svg" --font="$FONT" --color="$COLOR" \
    --margin="$MARGIN 0" $fontsize "$output/*.txt"
}

set -e
main "$@"
