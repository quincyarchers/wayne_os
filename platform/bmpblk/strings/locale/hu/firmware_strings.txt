A Chrome OS nincs telepítve vagy hibás.
Helyezzen be egy helyreállítási USB-tárat vagy SD-kártyát.
Helyezzen be egy helyreállítási USB-tárat.
Helyezzen be egy helyreállítási SD-kártyát vagy USB-tárat (megjegyzés: a kék USB-port NEM használható helyreállításhoz).
Helyezzen be egy helyreállítási USB-tárat az eszköz HÁTULJÁN található négy port valamelyikébe.
A csatlakoztatott eszközön nincs rajta a Chrome OS.
Operációsrendszer-ellenőrzés KIKAPCSOLVA
Nyomja meg a SZÓKÖZ billentyűt az újbóli engedélyezéshez.
Nyomja meg az ENTER billentyűt az operációsrendszer-ellenőrzés bekapcsolásának megerősítéséhez.
A rendszer újraindul, és a helyi adatok törlődnek.
A visszalépéshez nyomja meg az ESC billentyűt.
Operációsrendszer-ellenőrzés BEKAPCSOLVA.
Az operációsrendszer-ellenőrzés KIKAPCSOLÁSÁHOZ nyomja meg az ENTER billentyűt.
Ha segítségre van szüksége, látogasson el a https://google.com/chromeos/recovery webhelyre
Hibakód
Kérjük, távolítsa el az összes külső eszközt a helyreállítás megkezdéséhez.
60061e modell
Az operációsrendszer-ellenőrzés KIKAPCSOLÁSÁHOZ nyomja meg a VISSZAÁLLÍTÁS gombot.
A csatlakoztatott áramforrás nem elég nagy teljesítményű az eszköz működtetéséhez.
A Chrome OS kikapcsol.
Használja a megfelelő adaptert, és próbálja újra.
Távolítsa el az összes csatlakoztatott eszközt, és kezdje meg a helyreállítást.
Nyomja meg valamelyik számbillentyűt az alternatív rendszerindító kiválasztásához:
Nyomja meg a bekapcsológombot a diagnosztikai elemzés futtatásához.
Az operációs rendszer ellenőrzésének kikapcsolásához nyomja meg a BEKAPCSOLÁS gombot.
