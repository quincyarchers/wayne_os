Chrome OS లేదు లేదా పాడైంది.
దయచేసి పునరుద్ధరణ USB స్టిక్‌ లేదా SD కార్డ్‌ను చొప్పించండి.
దయచేసి పునరుద్ధరణ USB స్టిక్‌ను చొప్పించండి.
దయచేసి పునరుద్ధరణ SD కార్డ్‌ లేదా USB స్టిక్‌ను చొప్పించండి (గమనిక: నీలం రంగు USB పోర్ట్ పునరుద్ధరణకు పని చేయదు).
దయచేసి పునరుద్ధరణ USB స్టిక్‌ను డివైజ్ వెనుక భాగాన 4 పోర్ట్‌ల్లో ఒకదానిలో చొప్పించండి.
మీరు చొప్పించిన డివైజ్‌లో Chrome OS లేదు.
OS ధృవీకరణ ఆఫ్‌లో ఉంది
మళ్లీ ప్రారంభించడానికి స్పేస్‌ను నొక్కండి.
మీరు OS ధృవీకరణను ప్రారంభించాలనుకుంటున్నారని నిర్ధారించడానికి ENTERని నొక్కండి.
మీ సిస్టమ్ రీబూట్ అయ్యి స్థానిక డేటా తీసివేయబడుతుంది.
వెనుకకు వెళ్లడానికి, ESC నొక్కండి.
OS ధృవీకరణ ఆన్‌లో ఉంది.
OS ధృవీకరణను ఆఫ్ చేయడానికి, ENTERని నొక్కండి.
సహాయం కోసం https://google.com/chromeos/recoveryను సందర్శించండి
లోపం కోడ్
పునరుద్ధరణను ప్రారంభించడానికి దయచేసి బాహ్య డివైజ్‌లన్నీ తొలగించండి.
మోడల్ 60061e
OS ధృవీకరణను ఆఫ్ చేయడానికి, పునరుద్ధరణ బటన్‌ని నొక్కండి.
కనెక్ట్ అయిన విద్యుత్ సరఫరా ఈ డివైజ్‌కి సరిపడా విద్యుత్ శక్తిని కలిగి లేదు.
Chrome OS ఇప్పుడు షట్ డౌన్ అవుతుంది.
దయచేసి సరైన అడాప్టర్‌ను ఉపయోగించి, మళ్లీ ప్రయత్నించండి.
దయచేసి కనెక్ట్ అయిన డివైజ్‌లన్నీ తీసివేసి, పునరుద్ధరణను ప్రారంభించండి.
ప్రత్యామ్నాయ బూట్‌లోడర్‌ని ఎంచుకోవడానికి ఏదైనా ఒక సంఖ్యాత్మక కీని నొక్కండి:
సమస్య విశ్లేషణను అమలు చేయడానికి పవర్ బటన్‌ను నొక్కండి.
OS ధృవీకరణను ఆఫ్ చేయడానికి, పవర్ బటన్‌ను నొక్కండి.
